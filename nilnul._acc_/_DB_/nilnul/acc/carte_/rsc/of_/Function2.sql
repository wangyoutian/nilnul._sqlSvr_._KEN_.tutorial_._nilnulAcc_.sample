﻿CREATE FUNCTION [nilnul.acc.carte_.rsc.of_].[Keyword]
(
	@keyword as nvarchar(4000)
)
RETURNS TABLE 
AS RETURN
(
	select
		top 1000 
			*
			from 
				[nilnul.acc.carte_].Rsc
		where 
			uri like stuff(N'%%',2,0, @keyword) --'%eg%'
	
)
